<?php
namespace Deployer;

require 'recipe/common.php';

inventory('deploy.yml');

set('application', 'reformcity');
set('repository', 'git@bitbucket.org:novikovd/rc.git');
set('keep_releases', 3);
set('writable_mode', 'chown');

// Shared files/dirs between deploys
set('shared_files', [
    '.env',
]);

add('shared_dirs', [
    'storage',
    'app/frontend/runtime',
    'app/backend/runtime',
    'app/common/runtime',
    'app/console/runtime',
]);

// Writable dirs by web server
set('writable_dirs', [
    'storage',
    'app/frontend/runtime',
    'app/backend/runtime',
    'app/common/runtime',
    'app/console/runtime',
    'app/backend/web/assets',
    'app/frontend/web/assets',
]);

// Migrate database
task('database:migrate', function () {
    run('php {{release_path}}/yii migrate/up --interactive=0');
})->desc('Migrate database');

// Saving version of the application
task('save_version', function () {
    run ('cd {{release_path}} && git rev-parse --short HEAD > .version');
})->desc('Saving version');

// Links from storage to web
task('deploy:links', function () {
    run('ln -sf {{deploy_path}}/shared/storage/temp {{release_path}}/app/backend/web/temp');
    run('ln -sf {{deploy_path}}/shared/storage/public {{release_path}}/app/backend/web/storage');
    run('ln -sf {{deploy_path}}/shared/storage/public {{release_path}}/app/frontend/web/storage');
})->desc('Set links for data directory');

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:copy_dirs',
    'deploy:vendors',
    'database:migrate',
    'deploy:links',
    'save_version',
    'deploy:symlink',
    'cleanup'
])->desc('Deploy');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
