#!/bin/bash
# Install default application

yes | php yii migrate/down 999
yes | php yii migrate

# Creates default admin user (admin/admin)
php yii yiicom/user/create admin admin

# Creates default pages (index etc.)
php yii content/create/defaults

# Creates default catalog page
php yii catalog/create/defaults

# Creates default image preset 100x100
php yii files/create/default

# Creates symlinks storage/public -> backend/web/storage and frontend/web/storage
php yii yiicom/storage/create-public-links

# Creates symlink storage/temp -> backend/web/temp
php yii yiicom/storage/create-temp-link