# REFORM CITY website

### Установка и запуск контейнеров
Добавить **.env** файл:
```bash
$ cp .env.example .env
```

Настройки в файле **.env**:
```dotenv
# Настройки имен для docker хостов
DOCKER_FRONTEND_HOST=reform-city.docker
DOCKER_BACKEND_HOST=admin.reform-city.docker
```

Обновить файл **/etc/hosts**:
```text
127.0.1.1   reform-city.docker
127.0.1.1   admin.reform-city.docker
```

Выполнить команду:
```bash
$ make docker-up
```

Работают следующие хосты: 
- Frontend http://api-dev.bonus.docker
- Backend http://admin-dev.bonus.docker



### Доступные команды
```bash
$ make docker-up        # Запустить все контейнеры
$ make docker-down      # Остановить все контейнеры
$ make docker-build     # Пересобрать контейнеры (например при изменении конфига nginx)
$ make docker-rebuild   # Пересобрать все контейнеры с нуля
$ make ssh-php          # Консоль php контейнера с приложением 
```

### Docker containers
- PHP 7.4
- Mysql 5.7 
- Nginx 1.13 


