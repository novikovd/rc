import './sass/main.scss';

window._ = require('lodash');

import Vue from 'vue'
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios'
import VueAuth from '@websanova/vue-auth';
import BootstrapVue from 'bootstrap-vue';
import Notifications from 'vue-notification';
import VueCkeditor from 'vue-ckeditor2';
import VueUploadComponent from 'vue-upload-component';
import VueDraggable from 'vuedraggable';

// Yiicom module components
import axios from '../../../vendor/yiicom/yiicom/src/backend/assets/src/axios'
import App from '../../../vendor/yiicom/yiicom/src/backend/assets/src/components/App.vue';
import Loader from '../../../vendor/yiicom/yiicom/src/backend/assets/src/components/Loader.vue';
import AdminButtons from '../../../vendor/yiicom/yiicom/src/backend/assets/src/components/AdminButtons.vue';
import Debug from '../../../vendor/yiicom/yiicom/src/backend/assets/src/components/Debug.vue';

import commerceRoutes from '../../../vendor/yiicom/yiicom/src/backend/assets/src/routes/commerce.js';
import contentRoutes from '../../../vendor/yiicom/yiicom-content/src/backend/assets/src/routes/content.js';
import contentRelationGroupRoutes from '../../../vendor/yiicom/yiicom-content/src/backend/assets/src/routes/relationGroup.js';
import filesRoutes from '../../../vendor/yiicom/yiicom-files/src/backend/assets/src/routes/files.js';
import catalogCategoryRoutes from '../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/routes/category.js';
import catalogProductRoutes from '../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/routes/product.js';
import catalogAttributeRoutes from '../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/routes/attribute.js';
import catalogAttributeGroupRoutes from '../../../vendor/yiicom/yiicom-catalog/src/backend/assets/src/routes/attributeGroup.js';

import store from './store/store.js';

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(BootstrapVue);
Vue.use(Notifications);

Vue.component('file-upload', VueUploadComponent);
Vue.component('draggable', VueDraggable);
Vue.component('vue-ckeditor', VueCkeditor);
Vue.component('yc-loader', Loader);
Vue.component('yc-admin-buttons', AdminButtons);
Vue.component('yc-debug', Debug);

Vue.prototype._ = _;

const router = new VueRouter({
    mode: 'hash',
    linkActiveClass: 'active',
    routes: [
        ... commerceRoutes,
        ... contentRoutes,
        ... contentRelationGroupRoutes,
        ... filesRoutes,
        ... catalogCategoryRoutes,
        ... catalogProductRoutes,
        ... catalogAttributeRoutes,
        ... catalogAttributeGroupRoutes
    ],
});

Vue.router = router;

Vue.use(VueAuth, {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    loginData: {
        url: 'admin/api/v1/auth/login',
        redirect: '/',
        fetchUser: false
    },
    logoutData: {
        url: 'admin/api/v1/auth/logout',
        redirect: '/',
        makeRequest: true
    },
    refreshData: {
        url: 'admin/api/v1/auth/refresh',
        enabled: false,
        interval: 30
    },
    fetchData: {
        url: 'admin/api/v1/auth/user',
        enabled: false
    },
});

window.App = new Vue({
    store,
    router,
    render: h => h(App)
}).$mount('#app');
