const path = require('path');
const glob = require('glob');

const PATH = {
    src: path.join(__dirname, '../src'),
    dist: path.join(__dirname, '../dist')
};

module.exports = {
    externals: {
        path: PATH
    },
    entry: {
        app: PATH.src,
        sprite: glob.sync(path.resolve(__dirname, '../src/svg/**/*.svg'))
    },
    output: {
        filename: 'js/[name].js',
        path: PATH.dist,
        publicPath: '/'
    },
};