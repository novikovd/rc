<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'components' => [
        'session' => [
            'name' => 'REFORMCITY-BACKEND',
        ],
        'request' => [
            'cookieValidationKey' => getenv('BACKEND_COOKIE_VALIDATION_KEY'),
            'parsers' => [
                'application/json' => \yii\web\JsonParser::class,
            ]
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '/' => 'admin/main/default',
//                '<module:\w+>/<controller:\w+>/<action:\w+>' => 'admin/main/default',

                // TODO: combine the rules into one
                '<module:\w+>/api/v1/<controller:\w+>/<action:[\w-]+>' => '<module>/api/v1/<controller>/<action>',
                '<module:\w+>/api/v1/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/api/v1/<controller>/<action>',

                '<controller:\w+>/<action:\w+>' => '<controller>/<action>', // for elfinder route
            ],
        ],
        'user' => [
            'identityClass' => yiicom\backend\models\AdminUser::class,
        ],
        'view' => [
            'class' => yiicom\common\base\View::class,
            'theme' => [
                'pathMap' => [
                    '@yiicom' => '@app/themes/admin',
                ]
            ]
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => yiicom\backend\Module::class,
        ],
        'content' => [
            'class' => yiicom\content\backend\Module::class
        ],
        'catalog' => [
            'class' => yiicom\catalog\backend\Module::class
        ],
        'files' => [
            'class' => yiicom\files\backend\Module::class
        ],
    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => \mihaildev\elfinder\Controller::class,
            'access' => ['@'],
            'roots' => [
                [
                    'baseUrl' => '@backendWeb',
                    'basePath' => '@backendWebroot',
                    'path' => 'storage/uploads',
                    'name' => 'Файлы'
                ]
            ],
        ]
    ],
    'params' => [

    ],
];
