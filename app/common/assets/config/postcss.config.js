module.exports = ({ file, options, env }) => ({
    plugins: {
        'autoprefixer': {},
        // 'css-mqpacker': {},
        'cssnano': env === 'production' ? {
            preset: [
                'default', {
                    discardComments: {
                        removeAll: true
                    }
                }
            ]
        } : false
    }
});