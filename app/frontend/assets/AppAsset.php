<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @inheritdoc
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@frontend/assets/dist';

    public $css = [
        'css/app.css',
    ];

    public $js = [
        'js/vendors.js',
        'js/app.js',
    ];

    public $depends = [

    ];
}
