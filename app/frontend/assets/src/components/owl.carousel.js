let OwlCarousel = function () {
    return {
        options: {
            // Brands slider
            brands: {
                // center: true,
                loop: true,
                autoWidth: true,
                // margin: 16,
                // stagePadding: 20,
                nav: true,
                dots: false,
                navText: [
                    '<svg class="icon"><use xlink:href="#icon-arrow-left"></use></svg>',
                    '<svg class="icon"><use xlink:href="#icon-arrow-right"></use></svg>'
                ]
            }
        },

        /**
         * Init slider by selector
         * @param slider
         */
        init: function (slider) {
            let $slider = $(slider);
            let name = $slider.data('name');
            let options = {};

            if (typeof this.options[name] !== 'undefined') {
                options = this.options[name];
            }

            $slider.owlCarousel(options);
        },

        /**
         * Init all sliders on page
         */
        initAll: function () {
            let self = this;

            $('.owl-carousel').each(function (i, slider) {
                self.init(slider);
            });
        }

    };
};

module.exports = OwlCarousel;