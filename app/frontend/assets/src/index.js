import './sass/main.scss';

import 'bootstrap';
import 'owl.carousel';
import './vendors/fancybox/jquery.fancybox.min';

let App = require('./components/app');
let Format = require('./components/format');
let OwlCarousel = require('./components/owl.carousel');

window.app = new App();

app.format = new Format();

// app.product = new Product();
// app.product.init();

app.carousel = new OwlCarousel();
app.carousel.initAll();

app.init();


// import Vue from 'vue'
// import VueAxios from 'vue-axios'
// import axios from './components/axios.js'
// import CallbackForm from './components/form/CallbackForm.vue';
// import OrderForm from './components/form/OrderForm.vue';
// import AskQuestionForm from './components/form/AskQuestionForm.vue';
// import ReviewForm from './components/form/ReviewForm.vue';
// import store from './store/store.js';
//
// Vue.use(VueAxios, axios);
//
// Vue.component('callback-form', CallbackForm);
// Vue.component('order-form', OrderForm);
// Vue.component('ask-question-form', AskQuestionForm);
// Vue.component('review-form', ReviewForm);
//
// window.Vue = Vue;
// window.VueApp = new Vue({ store });