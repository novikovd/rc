<?php

return [
    'menuAbout' => require_once __DIR__ . '/menu/menu_about.php',
    'menuCatalog' => require_once __DIR__ . '/menu/menu_catalog.php',
    'menuCooperation' => require_once __DIR__ . '/menu/menu_cooperation.php',
    'menuServices' => require_once __DIR__ . '/menu/menu_services.php',
    'menuFooter' => require_once __DIR__ . '/menu/menu_footer.php',
];