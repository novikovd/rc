<?php

$menuCatalog = require __DIR__ . '/menu_catalog.php';
$menuAbout = require __DIR__ . '/menu_about.php';
$menuServices = require __DIR__ . '/menu_services.php';
$menuCooperation = require __DIR__ . '/menu_cooperation.php';

return [
    $menuCatalog,
    $menuAbout,
    $menuServices,
    $menuCooperation
];
