<?php

return [
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'session' => [
            'name' => 'REFORMCITY-FRONTEND',
        ],
        'request' => [
            'cookieValidationKey' => getenv('FRONTEND_COOKIE_VALIDATION_KEY'),
            'parsers' => [
                'application/json' => yii\web\JsonParser::class,
            ]

        ],
        'assetManager' => [
			'appendTimestamp' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [
                    'class' => yiicom\content\common\components\UrlRule::class,
                ],
                '<module:\w+>/api/<controller:[\w-]+>/<action:[\w-]+>' => '<module>/api/<controller>/<action>',
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'content/page/error',
        ],
        'view' => [
            'class' => yiicom\common\base\View::class,
            'theme' => [
                'pathMap' => [
                    '@yiicom/content' => '@app/themes/content',
                    '@yiicom/catalog' => '@app/themes/catalog',
                ]
            ]
        ],
        'user' => [
            'identityClass' => \yii\web\User::class,
        ],
    ],
    'modules' => [
        'content' => [
            'class' => yiicom\content\frontend\Module::class
        ],
        'catalog' => [
            'class' => yiicom\catalog\frontend\Module::class
        ],
    ],
    'params' => require_once __DIR__ . '/params.php',
];