<?php

use yii\web\View;
use yiicom\common\helpers\SvgIcon;
use yiicom\content\common\models\Page;
use yiicom\common\helpers\StringHelper;
use yiicom\files\common\widgets\ImageWidget;

/**
 * @var View $this
 * @var Page $page
 */

$params = \Yii::$app->params;

?>

<div class="poster mb-5">
    <img class="img-fluid" src="/images/examples/example_image_x.jpg" alt="" title="">
    <div class="col-lg-5 col-md-7 col-sm-12 poster__box poster__box--right">
        <div class="media-block text-center">
            <div class="media__category">Проект недели</div>
            <div class="media__line"></div>
            <div class="media__title">Гардеробная витра</div>
            <div class="media__teaser">
                Гардеробная комната Витра (Vitra) на алюминиевых стойках. Крепление пол/стена. Материал: Шпон<br>
                Америкнсокго Ореха, Тон И7 (натуральный).
            </div>
        </div>
    </div>
</div>

<div class="row catalog-widget mb-5">
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/cupboard.jpg" alt="" title="">
      <span>Шкафы</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/living_rooms.jpg" alt="" title="">
      <span>Гостинные</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/hallways.jpg" alt="" title="">
      <span>Прихожие</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/children.jpg" alt="" title="">
      <span>Детские</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/badrooms.jpg" alt="" title="">
      <span>Спальни</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/cabinets.jpg" alt="" title="">
      <span>Кабинеты</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/bathrooms.jpg" alt="" title="">
      <span>Ванные</span>
    </a>
  </div>

  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/dressing_rooms.jpg" alt="" title="">
      <span>Гардеробные</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/kitchens.jpg" alt="" title="">
      <span>Кухни</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/office.jpg" alt="" title="">
      <span>Офис</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/reception.jpg" alt="" title="">
      <span>Ресепшен</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/walls.jpg" alt="" title="">
      <span>Стеновые панели</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/acrylic.jpg" alt="" title="">
      <span>Изделия из акрила</span>
    </a>
  </div>
  <div class="catalog-widget__col">
    <a class="catalog-widget__category" href="#">
      <img src="/images/catalog/other.jpg" alt="" title="">
      <span>Разное</span>
    </a>
  </div>

  <div class="w-100 text-center mt-2">
    <a class="btn btn-primary btn--md" href="#">Перейти в каталог</a>
  </div>

  <div class="catalog-widget__bg"></div>
</div>

<div class="row mb-5">
    <div class="col-sm-12 col-md-6 col-lg-4">
        <a class="poster-sm" href="#">
            <img src="/images/examples/example_banner_01.jpg" alt="" title="">
            <div class="poster__box">
                <div class="poster__title">Скидка 35%</div>
                <div class="poster__line"></div>
                <div class="poster__text">На гардеробные Vitra</div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <a class="poster-sm" href="#">
            <img src="/images/examples/example_banner_02.jpg" alt="" title="">
            <div class="poster__box">
                <div class="poster__title">Скидка 35%</div>
                <div class="poster__line"></div>
                <div class="poster__text">На гардеробные Vitra</div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
        <a class="poster-sm" href="#">
            <img src="/images/examples/example_banner_03.jpg" alt="" title="">
            <div class="poster__box">
                <div class="poster__title">Выезд дизайнера интерьера</div>
                <div class="poster__line"></div>
                <div class="poster__text">
                    Персональная консультация<br>
                    <b>Возможен бесплатный выезд</b>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-8">
        <div class="media-block text-center">
            <img src="/images/examples/example_image_l.jpg" alt="" title="">
            <div class="media__category">Карта покупок</div>
            <div class="media__line"></div>
            <div class="media__title">Спальня из шпона американского ореха</div>
            <div class="media__teaser">
                Гардеробная комната Витра (Vitra) на алюминиевых стойках. Крепление пол/стена. Материал: Шпон<br>
                Америкнсокго Ореха, Тон И7 (натуральный).
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12 col-lg-4">
        <div class="row">
            <div class="col-sm-6 col-lg-12">
                <div class="media-block text-left">
                    <img src="/images/examples/example_image_m.jpg" alt="" title="">
                    <div class="media__category">Советы</div>
                    <div class="media__line"></div>
                    <div class="media__title">Белый шкаф - легко<br> и сильно</div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-12">
                <div class="media-block text-left">
                    <img src="/images/examples/example_image_m.jpg" alt="" title="">
                    <div class="media__category">Карта покупок</div>
                    <div class="media__line"></div>
                    <div class="media__title">Стеновые панели на заказ</div>
                </div>
            </div>
        </div>
    </div>

</div>