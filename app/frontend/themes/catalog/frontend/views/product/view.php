<?php

use yii\helpers\Html;
use yiicom\common\base\View;
use yiicom\catalog\common\models\Product;
use yiicom\catalog\common\models\Category;
use yiicom\catalog\common\models\AttributeType;
use yiicom\common\helpers\SvgIcon;
use yiicom\files\common\models\File;
use yiicom\files\common\widgets\ImageWidget;

/**
 * @var View $this
 * @var Product $product
 * @var File[] $images
 * @var Category $category
 * @var Category[] $categoryParents
 * @var array $attributes
 * @var array $relations
 */

foreach ($categoryParents as $parent) {
    $this->params['breadcrumbs'][] = [
        'label' => Html::encode($parent->title ?: $parent->name),
        'url' => "/{$parent->url->alias}"
    ];
}

$this->params['breadcrumbs'][] = [
    'label' => Html::encode($category->title ?: $category->name),
    'url' => "/{$category->url->alias}"
];
$this->params['breadcrumbs'][] = Html::encode($product->title ?: $product->name);

?>

<div class="product">

    <h1><?php echo Html::encode($product->title ?: $product->name); ?></h1>

    <div class="product___count">1865 товаров</div>

    <div class="product___logos">
        <?= new SvgIcon('product-quality', '58x58') ?>
        <?= new SvgIcon('product-euro', '58x58') ?>
        <?= new SvgIcon('product-delivery', '58x58') ?>
        <?= new SvgIcon('product-furniture', '58x58') ?>
        <?= new SvgIcon('product-save', '58x58') ?>
        <?= new SvgIcon('product-eko', '58x58') ?>
        <?= new SvgIcon('product-car', '58x58') ?>
    </div>

    <div class="row">
        <div class="col-md-7 mb-5 product__image-wrap">
            <div class="product__image">
              <div class="img"></div>
<!--                --><?php //if (isset($images[0])) : ?>
<!--                    --><?php //echo ImageWidget::widget([
//                        'images' => $images[0],
//                        'preset' => '576x426',
//                        'linkPreset' => '1200x900',
//                        'linkOptions' => [
//                            'class' => 'product__image-link',
//                            'data-fancybox' => 'gallery',
//                        ],
//                        'options' => ['class' => 'img-fluid']
//                    ]); ?>
<!--                --><?php //endif; ?>
            </div>
        </div>

        <div class="col-md-5 mb-5">

            <?php if ($product->isShowPrice) : ?>
                <div class="product__price">Стоимость аренды: от <span><?= $product->price ?></span> руб/час</div>
            <?php endif; ?>

<!--            --><?//= $this->render('../product/_attributes', [
//                'product' => $product,
//                'attributes' => $attributes,
//                'isShowInProduct' => true,
//            ]); ?>

            <div class="product__equipment-link">
                <a class="link" href="#" data-scroll-to=".product__equipment">Комплектация</a>
            </div>

            <a class="btn btn-primary btn-lg product__btn-rent" href="/arendovat?productId=<?= $product->id; ?>"
               title="Оформить предварительный заказ или расчитать стоимость поездки">Заказать</a>

        </div>

    </div>

<!--    --><?php //if (count($images) > 1) : ?>
<!--        <div class="row mb-5">-->
<!--            <div class="col-md-12">-->
<!--                <div class="owl-carousel product__images" data-name="product">-->
<!--                    --><?php //echo ImageWidget::widget([
//                        'images' => $images,
//                        'preset' => '281x207',
//                        'dataPreset' => '576x426',
//                        'linkPreset' => '1200x900',
//                        'linkOptions' => [
//                            'class' => 'product__img-link',
//                            'data-fancybox' => 'gallery'
//                        ],
//                        'options' => ['class' => 'product__img']
//                    ]); ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    --><?php //endif; ?>

    <?php if ($product->teaser) : ?>
        <div class="row">
            <div class="col-md-12 product__teaser">
                <?= $product->teaser ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($product->body) : ?>
        <div class="row">
            <div class="col-md-12 product__body">
                <h2>Описание</h2>
                <?= $product->body ?>
            </div>
        </div>
    <?php endif; ?>

<!--    --><?php //if (isset($attributes['equipment']['attributes'])) : ?>
<!--        <div class="row">-->
<!--            <div class="col-md-12 product__equipment">-->
<!--                <h2>Комплектация</h2>-->
<!--                <ul class="attributes">-->
<!--                    --><?php //foreach ($attributes['equipment']['attributes'] as $attribute) : ?>
<!--                        --><?php //if (isset($product->attributeValue->value[$attribute['id']])) : ?>
<!--                            <li class="attribute">-->
<!--                                <span>--><?//= Html::encode($attribute['title']) ?><!--</span>: есть-->
<!--                            </li>-->
<!--                        --><?php //endif; ?>
<!--                    --><?php //endforeach; ?>
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->
<!--    --><?php //endif; ?>

    <?php if ($relations) : ?>

        <div class="relations mt-4">

            <div class="row">
                <?php if (isset($relations['productPopular'])) : ?>
                    <div class="col-md-6 col-sm-12 col-12">
                        <h3 class="h2"><?= Html::encode($relations['productPopular']->title); ?></h3>
                        <div class="row">
                            <?= $this->render('_cards', [
                                'products' => $relations['productPopular']->relations,
                                'attributes' => $attributes,
                            ]); ?>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (isset($relations['productInteresting'])) : ?>
                    <div class="col-md-6 col-sm-12 col-12">
                        <h3 class="h2"><?= Html::encode($relations['productInteresting']->title); ?></h3>
                        <div class="row">
                            <?= $this->render('_cards', [
                                'products' => $relations['productInteresting']->relations,
                                'attributes' => $attributes,
                            ]); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>

            <?php if (isset($relations['productAlsoLook'])) : ?>
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="h2"><?= Html::encode($relations['productAlsoLook']->title); ?></h3>
                        <div class="row">
                            <?= $this->render('_cards', [
                                'products' => $relations['productAlsoLook']->relations,
                                'attributes' => $attributes,
                            ]); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

        </div><!-- /.relations -->

    <?php endif; ?>


</div><!-- /product -->