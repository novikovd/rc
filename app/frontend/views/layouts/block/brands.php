<?php
?>

<div class="brands">
  <div class="container">
    <div class="row">

      <div class="col-lg-2 col-md-3">
        <h3 class="brands__title">Нам доверяют:</h3>
      </div>

      <div class="col-lg-10 col-md-9">
        <div class="brands__wrap">
          <div class="owl-carousel" data-name="brands">
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
            <a class="" href="#" data-fancybox="gallery">
              <img class="brands__img" src="/images/examples/example_brand.png" alt="М" title="">
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
