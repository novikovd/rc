<?php

use yii\helpers\Html;
use yii\web\View;
use frontend\assets\AppAsset;
use yiicom\common\helpers\SvgIcon;

/**
 * @var View $this
 * @var string $content
 * @var array $params
 */

$this->registerAssetBundle(AppAsset::class);
$params = \Yii::$app->params;

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo Html::csrfMetaTags(); ?>
    <title><?php echo Html::encode($this->title); ?></title>
    <?php echo $this->head(); ?>
    <?php echo $this->render('_favicon'); ?>
</head>
<body>
<?php $this->beginBody(); ?>

    <div class="wrapper">

        <div class="container">
            <?php echo $this->render('_header'); ?>
        </div>

        <?php echo $this->render('_menu_main'); ?>

        <div class="container container--main">

            <?php echo $this->render('_breads', [
                'breads' => $this->params['breadcrumbs'] ?? []
            ]); ?>

            <div class="row">
                <div class="col-12">
                    <div class="content">
                        <?= $content; ?>
                    </div>
                </div>
            </div>

            <?= $this->render('menu/left', []) ?>

        </div><!-- /.container--main -->

        <?= $this->render('block/brands'); ?>
        <?= $this->render('menu/bottom'); ?>
        <?= $this->render('menu/footer', ['menus' => $params['menuFooter']]); ?>

        <?= $this->render('_footer', ['params' => $params]); ?>

    </div>

	  <?= $this->render('_scripts_bottom'); ?>

<?php $this->endBody(); ?>

</body>
</html>
<?php $this->endPage(); ?>
