<?php

use yiicom\common\helpers\StringHelper;

/**
 * @var array $params
 */

?>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

              © 1996 - <?= date('Y') ?> "РЕФОРМ СИТИ" - мебель на заказ по индивидуальных проектам<br>
              Адрес: <?= $params['address']['full'] ?><br>
              Телефон:
              <a href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['primary']); ?>" title="Позвонить по телефону">
                <?= $params['phone']['primary'] ?>
              </a>,
              <a href="tel:+<?= StringHelper::toNumber(Yii::$app->params['phone']['secondary']); ?>" title="Позвонить по телефону">
                  <?= $params['phone']['secondary'] ?>
              </a>
              <br>
              Email: <a href="mailto:<?= $params['email']['primary'] ?>"><?= $params['email']['primary'] ?></a>
            </div>
        </div>
    </div>
</footer>
