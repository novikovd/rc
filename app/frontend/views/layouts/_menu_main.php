<?php

use yiicom\common\helpers\SvgIcon;

?>

<div class="menu-main">

    <div class="container">

        <div class="row no-gutters">

            <div class="col-2">
                <div class="menu-main__search">
                    <a class="link-default" href="#">
                        <?= new SvgIcon('loop-bold', 'sm') ?>
                    </a>
                </div>
            </div>

            <div class="col-10">

                <ul class="menu-main__menu">
                    <li class="menu-main__item">
                        <a class="link-default" href="#">
                            <?= new SvgIcon('shop-bold', 'sm') ?><span>Магазин</span>
                        </a>
                    </li>
                    <li class="menu-main__item">
                        <a class="link-default" href="#">
                            <?= new SvgIcon('idea-bold', 'sm') ?><span>Идеи интерьеров</span>
                        </a>
                    </li>
                    <li class="menu-main__item">
                        <a class="link-default" href="#">
                            <?= new SvgIcon('faq-bold', 'sm') ?><span>Советы</span>
                        </a>
                    </li>
                    <li class="menu-main__item">
                        <a class="link-default" href="#">
                            <?= new SvgIcon('user-bold', 'sm') ?><span>Покупателю</span>
                        </a>
                    </li>
                    <li class="menu-main__item">
                        <a class="link-default" href="#">
                            <?= new SvgIcon('design-bold', 'sm') ?><span>Дизайнеру</span>
                        </a>
                    </li>
                    <li class="menu-main__item">
                        <a class="link-default" href="#">
                            <?= new SvgIcon('star-new-bold', 'sm') ?><span>Новинки</span>
                        </a>
                    </li>
                </ul>

            </div>

        </div>

    </div><!-- /.container -->

</div><!-- /.menu-main -->
