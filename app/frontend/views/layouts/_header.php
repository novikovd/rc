<?php

?>

<header class="header">

    <div class="row">

        <div class="col-2">
            <a class="header__logo" href="/">
                <img src="/images/logo_reform_city.png"
                     ale="Компания РЕФОРМ СИТИ - изготовление мебели по индивидуальным проектам в Москве"
                     title="Компания РЕФОРМ СИТИ - изготовление мебели по индивидуальным проектам">
            </a>
        </div>

        <div class="col-10">

            <div class="row">

                <div class="col-10">

                    <div clamain-menuss="row">
                        <div class="col-12">
                            <ul class="header__menu menu-top">
                                <li><a class="link-default" href="#">Шоу-рум</a></li>
                                <li><a class="link-default" href="#">Контакты</a></li>
                                <li><a class="link-default" href="#">Условия покупки</a></li>
                                <li><a class="link-default" href="#">Обратная связь</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row no-gutters">
                        <div class="col-3 offset-md-2">
                            <div class="header__phone">+7 (495) 943-63-15</div>
                            <div class="header__phone">+7 (903) 675-91-48</div>
                        </div>
                        <div class="col-2">
                            <div class="header__time">пн-пт: 09.00 - 21.00</div>
                            <div class="header__time">сб-вс: 10.00 - 19.00</div>
                        </div>
                        <div class="col-5 text-right">
                            <a class="btn btn-primary header__action" href="#">Выслать проект</a>
                            <a class="btn btn-primary header__action" href="#">Вызвать дизайнера</a>
                        </div>
                    </div>

                </div>

                <div class="col-2">
                    <a class="header__user" href="#"></a>

                    <div class="cart header__cart">
                        <div class="cart__title">Корзина</div>
                        <div class="cart__amount">0 Р</div>
                        <div class="cart__icon"></div>
                    </div>
                </div>

            </div>



        </div>

    </div>

</header>
