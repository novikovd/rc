<?php

use yiicom\common\helpers\SvgIcon;

?>

<div class="menu menu-left">
    <div class="menu__title">Меню</div>

    <a class="menu__user" href="#">
        <img src="/images/examples/user_pic.jpg" alt="" title="">
    </a>

    <ul class="menu__menu">
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('cart', 'md') ?>
                <span>Корзина</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('star-favorite', 'md') ?>
                <span>избранное</span>
            </a>
        </li>
    </ul>

    <ul class="menu__menu">
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('loop', 'md') ?>
                <span>Искать</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('shop', 'md') ?>
                <span>Каталог</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('table', 'md') ?>
                <span>Каталог</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('idea', 'md') ?>
                <span>Каталог</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('faq', 'md') ?>
                <span>Каталог</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('user', 'md') ?>
                <span>Каталог</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('design', 'md') ?>
                <span>Каталог</span>
            </a>
        </li>
        <li class="menu__item">
            <a class="menu__link" href="#">
                <?= new SvgIcon('star-new', 'md') ?>
                <span>Каталог</span>
            </a>
        </li>
    </ul>

    <a class="menu__back" href="#">Наверх</a>

</div>
