<?php

use yiicom\common\helpers\SvgIcon;

?>

<div class="row up-button">
  <a class="up-button__btn" href="#">
    <?= new SvgIcon('arrow-up') ?>
    <span>Наверх</span>
  </a>
  <div class="up-button__bg"></div>
</div>

<div class="row">
    <div class="col-md-12">

        <ul class="menu menu-bottom">
            <li class="menu__item">
                <a class="menu__link link-default" href="#">О компании</a>
            </li>
            <li class="menu__item">
                <a class="menu__link link-default" href="#">Торговые марки</a>
            </li>
            <li class="menu__item">
                <a class="menu__link link-default" href="#">Дизайнерам</a>
            </li>
            <li class="menu__item">
                <a class="menu__link link-default" href="#">Проекты</a>
            </li>
            <li class="menu__item">
                <a class="menu__link link-default" href="#">Новости</a>
            </li>
            <li class="menu__item">
                <a class="menu__link link-default" href="#">Оплата и доставка</a>
            </li>
            <li class="menu__item">
                <a class="menu__link link-default" href="#">Контакты</a>
            </li>
        </ul>

    </div>
</div>
