<?php

/**
 * @var array $menus
 */

?>

<div class="menu-footer">
  <div class="container">
    <div class="row">
      <?php foreach ($menus as $menu) : ?>
        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-2 menu-footer__menu">
          <a class="menu__title" href="<?= $menu['link'] ?>"><?= $menu['text'] ?></a>
          <ul class="menu__list">
            <?php foreach ($menu['items'] as $item) : ?>
              <li class="menu__item<?= $item['link'] === $this->pathInfo ? ' active' : '' ?>">
                <a class="menu__link" href="/<?= $item['link'] ?>"><?= $item['text'] ?></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>